#include "mainthread.h"
#include <QFile>
#include <QVariant>
#include <QDateTime>
#include <QMetaObject>

#define FILENAME "configuration.xml"

MainThread::MainThread(QObject *rootComponent, QObject *parent) : QObject(parent)
{
    this->rootComponent = rootComponent;

    this->initializeFilesystemWatcher();

    this->loadConfiguration(FILENAME);
}

MainThread::~MainThread()
{
    delete this->filesystemWatcher;
}

void MainThread::initializeFilesystemWatcher()
{
    this->filesystemWatcher = new QFileSystemWatcher();

    this->filesystemWatcher->addPath(FILENAME);

    connect(this->filesystemWatcher, SIGNAL(fileChanged(QString)), this, SLOT(loadConfiguration(QString)));
}

void MainThread::loadConfiguration(const QString &path)
{
    QDomDocument domDocument = this->loadDocument(path);

    QDomElement root = domDocument.documentElement();
    QDomNodeList titles = root.elementsByTagName("Title");
    QDomNodeList expireTimes = root.elementsByTagName("ExpireTime");

    QDomElement title = titles.at(0).toElement();
    this->rootComponent->setProperty("timeoutTitle", title.text());
    this->rootComponent->setProperty("timeoutTitlePixelSize", title.attribute("pixelSize"));

    QDomElement expireTime = expireTimes.at(0).toElement();
    this->rootComponent->setProperty("timeoutDate", QDateTime::fromString(expireTime.text(), "yyyy-MM-dd HH:mm:ss"));
    this->rootComponent->setProperty("timeoutDatePixelSize", title.attribute("pixelSize"));
}

QDomDocument MainThread::loadDocument(const QString &path)
{
    QFile file(path);
    if(!file.open(QIODevice::ReadOnly))
    {
        return QDomDocument();
    }

    QString errorMessage;
    int errorLine;
    int errorColumn;
    QDomDocument domDocument;
    if(!domDocument.setContent(&file, &errorMessage, &errorLine, &errorColumn))
    {
        file.close();
        return QDomDocument();
    }

    file.close();
    return domDocument;
}
