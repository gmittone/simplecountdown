#ifndef MAINTHREAD_H
#define MAINTHREAD_H

#include <QObject>
#include <QDomDocument>
#include <QFileSystemWatcher>

class MainThread : public QObject
{
    Q_OBJECT
public:
    explicit MainThread(QObject *rootComponent, QObject *parent = nullptr);
    virtual ~MainThread();

signals:

public slots:

private:
    QObject *rootComponent;
    QFileSystemWatcher *filesystemWatcher;

    void initializeFilesystemWatcher();
    QDomDocument loadDocument(const QString &path);

private slots:
    void loadConfiguration(const QString &path);
};

#endif // MAINTHREAD_H
