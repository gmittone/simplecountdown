import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.4
import "components"

Window {
    property int timeoutTitlePixelSize: 80
    property string timeoutTitle: ''
    property date timeoutDate: new Date();
    property int timeoutDatePixelSize: 56

    id: mainWindow
    visible: true
    x: (Screen.width / 2) - (width / 2);
    y: (Screen.height / 2) - (height / 2);
    height: Math.max(600, root.height)
    width: Math.max(800, root.width)
    maximumHeight: Screen.height
    maximumWidth: Screen.width
    flags: Qt.Window | Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint | Qt.WindowTransparentForInput | Qt.WindowDoesNotAcceptFocus
    title: qsTr('Simple Countdown')
    color: 'transparent'

    Item {
        id: root
        anchors.centerIn: parent
        height: description.height + 5 + boxContatore.height + 40
        width: Math.max(description.width, boxContatore.width) + 40

        Rectangle {
            anchors.fill: parent
            radius: 40
            color: '#e6e6e6'
            opacity: 0.4
        }

        Label {
            id: description
            anchors.top: root.top
            anchors.horizontalCenter: root.horizontalCenter
            text: timeoutTitle
            font.pixelSize: timeoutTitlePixelSize
            font.bold: true
        }

        Item {
            id: boxContatore
            anchors.top: description.bottom
            anchors.topMargin: timeoutDatePixelSize
            anchors.horizontalCenter: root.horizontalCenter
            height: segnoId.height
            width: segnoId.width + 5 + giorniId.width + 5 + oreId.width + 5 + minutiId.width + 5 + secondiId.width

            Cifra {
                id: segnoId
                anchors.top: boxContatore.top
                anchors.left: boxContatore.left
                pixelSize: timeoutDatePixelSize
                model: [' ','-','+']
                description: ''
            }

            Cifra {
                id: giorniId
                anchors.top: boxContatore.top
                anchors.left: segnoId.right
                pixelSize: timeoutDatePixelSize
                model: 1000
                description: qsTr('Giorni')
            }

            Cifra {
                id: oreId
                anchors.top: boxContatore.top
                anchors.left: giorniId.right
                pixelSize: timeoutDatePixelSize
                model: 24
                description: qsTr('Ore')
            }

            Cifra {
                id: minutiId
                anchors.top: boxContatore.top
                anchors.left: oreId.right
                pixelSize: timeoutDatePixelSize
                model: 60
                description: qsTr('Minuti')
            }

            Cifra {
                id: secondiId
                anchors.top: boxContatore.top
                anchors.left: minutiId.right
                pixelSize: timeoutDatePixelSize
                model: 60
                description: qsTr('Secondi')
            }
        }
    }

    Timer {
        interval: 1000
        running: true
        repeat: true
        onTriggered: setValues()
    }

    function setValues() {
        var data = new Date();
        var differenza = Math.floor((data.getTime() - timeoutDate.getTime()) / 1000);
        var segno;
        if(differenza == 0) {
            segno = 0;
        } else if(differenza < 0) {
            segno = 1;
        } else {
            segno = 2;
        }

        differenza = Math.abs(differenza);
        var secondi = differenza % 60;
        differenza /= 60;
        var minuti = differenza % 60;
        differenza /= 60;
        var ore = differenza % 24;
        differenza /= 24;
        var giorni = differenza;

        if(giorni > 999) {
            ore = 23;
            minuti = 59;
            secondi = 59;
        }

        segnoId.currentIndex = segno;
        giorniId.currentIndex = giorni;
        oreId.currentIndex = ore;
        minutiId.currentIndex = minuti;
        secondiId.currentIndex = secondi;
    }
}
