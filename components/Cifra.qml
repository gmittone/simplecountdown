import QtQuick 2.0
import QtQuick.Controls 2.4

Item {
    property string description: ''
    property int pixelSize: 20
    property alias model: tumblerId.model
    property alias currentIndex: tumblerId.currentIndex
    property int tumblerSize: 1

    id: root
    height: labelId.height + 5 + tumblerId.height
    width: tumblerId.width

    Label {
        id: labelId
        anchors.top: root.top
        anchors.horizontalCenter: tumblerId.horizontalCenter
        text: description
        font.pixelSize: pixelSize / 2
    }

    Tumbler {
        id: tumblerId
        anchors.top: labelId.bottom
        anchors.topMargin: 5
        anchors.left: root.left
        anchors.leftMargin: 5
        height: pixelSize * 2.5
        width: pixelSize * tumblerSize
        visibleItemCount: 1
        delegate: tumblerDelegate
        wrap: true

        onCountChanged: calculateSize()
    }

    Component {
        id: tumblerDelegate

        Label {
            text: formatText(Tumbler.tumbler.count, modelData)
            opacity: 1.0 - Math.abs(Tumbler.displacement) / (Tumbler.tumbler.visibleItemCount / 2)
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: pixelSize
        }
    }

    function calculateSize() {
        if(tumblerId.count < 10) {
            tumblerSize = 1;
        } else if(tumblerId.count < 100) {
            tumblerSize = 2;
        } else {
            tumblerSize = 3;
        }
    }

    function formatText(count, modelData) {
        var modelText = modelData.toString();

        while(modelText.length < tumblerSize) {
            modelText = '0' + modelText;
        }

        return modelText;
    }
}
